#!/bin/bash
# start from loop.sh
# we remove first ip address from list - beacuse there might be dynamic rule already.

#/ip firewall address-list add address=188.124.54.226/32 list=allow-ip
#/user set address=192.168.3.0/24,192.168.1.0/24,192.168.0.0/24,90.189.213.100/32,5.128.62.98/32,109.174.97.50/32,217.70.122.160/32,87.103.250.138/32,90.189.159.14/32,90.189.145.25/32,176.9.85.2/32,183.89.239.83/32,37.192.46.66/32,188.124.54.226/32 [find name!="lollo"]
ssh -o ConnectTimeout=1 -o ConnectionAttempts=1 -o "StrictHostKeyChecking=no" -T $1 2>&1 << EOF
/ip firewall address-list remove [find address="5.130.85.30"]
/ip firewall address-list remove [find address="195.16.43.114"]
/ip firewall address-list add address=5.130.85.30/32 list=allow-ip
/ip firewall address-list add address=195.16.43.114/32 list=allow-ip
/quit
EOF

#/ip firewall address-list add address=109.252.236.206/32 list=allow-ip
#/ip firewall address-list add address=5.130.85.30/32 list=allow-ip
#/ip firewall address-list add address=195.16.43.114/32 list=allow-ip
#/ip service set winbox disabled=yes
#/ip firewall address-list remove [find address="37.192.46.66"]
#/user set address=192.168.3.0/24,192.168.1.0/24,192.168.0.0/24,90.189.213.100/32,5.128.62.98/32,109.174.97.50/32,217.70.122.160/32,87.103.250.138/32,90.189.159.14/32,90.189.145.25/32,176.9.85.2/32,183.89.239.83/32,37.192.46.66/32,188.124.54.226/32 [find name!="lollo"]
