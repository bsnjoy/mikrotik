#!/bin/bash
# Обновить список hostlist.txt
# cat /root/.ssh/mikrotik | grep "Host " | awk '{print $2}' > hostlist.txt && sed -ri"" '/mihome.+/d' hostlist.txt
for host in $(cat hostlist.txt)
do
	echo $host
	./add-allowip.sh $host
done
